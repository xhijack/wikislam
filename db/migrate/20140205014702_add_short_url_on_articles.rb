class AddShortUrlOnArticles < ActiveRecord::Migration
  def up
	add_column :articles, :shorturl, :string
  end

  def down
	remove_column :articles, :shorturl
  end
end
