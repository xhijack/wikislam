class AddFieldLastScrapeRssSource < ActiveRecord::Migration
  def up
	add_column :rss_sources,:last_scrape,:datetime
  end

  def down
	remove_column :rss_sources, :last_scrape
  end
end
