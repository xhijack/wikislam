class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title
      t.string :content
      t.string :summary
      t.string :image_url
      t.string :site_url
      t.integer :rss_source_id
      t.integer :status
      t.integer :order

      t.timestamps
    end
  end
end
