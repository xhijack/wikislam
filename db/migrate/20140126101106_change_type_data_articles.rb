class ChangeTypeDataArticles < ActiveRecord::Migration
  def up
	change_column :articles, :published, :datetime
  end

  def down
	change_column :articles, :published, :string
  end
end
