class CreateRssSources < ActiveRecord::Migration
  def change
    create_table :rss_sources do |t|
      t.string :name
      t.string :url
      t.integer :trusted
      t.string :tag
      t.integer :status

      t.timestamps
    end
  end
end
