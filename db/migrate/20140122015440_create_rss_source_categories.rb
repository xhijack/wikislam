class CreateRssSourceCategories < ActiveRecord::Migration
  def change
    create_table :rss_source_categories do |t|
      t.integer :category_id
      t.integer :rss_source_id

      t.timestamps
    end
  end
end
