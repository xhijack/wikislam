# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#RssSource.create(name: 'muslim.or.id', url: 'http:muslim.or.id/feed', trusted: 1, status: 1)
#RssSource.create(name: 'Dakwatuna', url: 'http://dakwatuna.com/feed', trusted: 1, status: 1)
#RssSource.create(name: 'Underground Tauhid', url: 'http://undergroundtauhid.com/feed', trusted: 1, status: 1)
#RssSource.create(name: 'Arrahmah', url: 'http://www.arrahmah.com/feed', trusted: 1, status: 1)
#RssSource.create(name: 'Voa Islam', url: 'http://voa-islam.com/rss', trusted: 1, status: 1)
#RssSource.create(name: 'Era Muslim', url: 'http://www.eramuslim.com/feed', trusted: 1, status: 1)
#RssSource.create(name: 'Hidayatullah', url: 'http://www.hidayatullah.com/feed/', trusted: 1, status: 1)
#RssSource.create(name: 'Fimadani', url: 'http://www.fimadani.com/feed/', trusted: 1, status: 1)
#RssSource.create(name: 'Asysyariah', url: 'http://asysyariah.com/feed/', trusted: 1, status: 1)
#RssSource.create(name: 'Islam Pos', url: 'http://www.islampos.com/feed', trusted: 1, status: 1)
#RssSource.create(name: 'Konsultasi Syariah', url: 'http://www.konsultasisyariah.com/feed/', trusted: 1, status: 1)
#RssSource.create(name: 'Hizbut Tahrir', url: 'http://hizbut-tahrir.or.id/feed', trusted: 1, status: 1)
##RssSource.create(name: 'Pengusaha Muslim', url: 'http://pengusahamuslim.com/rss', trusted: 1, status: 1)
##RssSource.create(name: 'Nahi Munkar', url: 'http://www.nahimunkar.com/rss', trusted: 1, status: 1)
#RssSource.create(name: 'Rumasyho', url: 'http://rumaysho.com/feed', trusted: 1, status: 1)
#RssSource.create(name: 'Suara Islam', url: 'http://suara-islam.com/rss', trusted: 1, status: 1)
Article.find_each(&:save)