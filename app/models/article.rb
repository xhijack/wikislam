class Article < ActiveRecord::Base
  extend FriendlyId
  
  attr_accessible :counter,:content, :shorturl,:image_url, :order, :rss_source_id, :site_url, :status, :summary, :title
  belongs_to :rss_source
  validates :title, uniqueness: true



  friendly_id :title, use: :slugged
end
