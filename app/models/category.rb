class Category < ActiveRecord::Base
  attr_accessible :name, :status
  has_many :article_category
  has_many :article, through: :article_category
end
