class ArticlesController < ApplicationController
  # GET /articles
  # GET /articles.json
  theme 'mobile'
  def index
    #    @articles = Article.order("published asc")
    #
    #    respond_to do |format|
    #      format.html # index.html.erb
    #      format.json { render json: @articles }
    #    end
  end

  def list
    if params[:keyword].blank?
      if params[:rss_source_id].blank?
        @articles = Article.order("published desc").page(params[:page]).per(20)
      else
        @articles = Article.where(rss_source_id: params[:rss_source_id]).order("published desc").page(params[:page]).per(20)
      end
    else
      @articles = Article.where("title like ? or content like ?","%#{params[:keyword]}%","%#{params[:keyword]}%").order("published desc").page(params[:page]).per(20)
    end
  end


  def resources
    @rsssources = RssSource.where(status: 1)
  end

  def show_article
    @article = Article.find_by_slug(params[:id])
    self.count(params[:id])
  end

  def count(id)
    article = Article.find_by_slug(id)
    if article.nil?
      article.counter = 1
      article.save
    else
      article.counter = article.counter.to_i + 1
      article.save
    end
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
    @article = Article.find_by_slug(params[:id])
    self.count(params[:id])
    render "show_article"
  end
  #
  #  # GET /articles/new
  #  # GET /articles/new.json
  #  def new
  #    @article = Article.new
  #
  #    respond_to do |format|
  #      format.html # new.html.erb
  #      format.json { render json: @article }
  #    end
  #  end
  #
  #  # GET /articles/1/edit
  #  def edit
  #    @article = Article.find(params[:id])
  #  end
  #
  #  # POST /articles
  #  # POST /articles.json
  #  def create
  #    @article = Article.new(params[:article])
  #
  #    respond_to do |format|
  #      if @article.save
  #        format.html { redirect_to @article, notice: 'Article was successfully created.' }
  #        format.json { render json: @article, status: :created, location: @article }
  #      else
  #        format.html { render action: "new" }
  #        format.json { render json: @article.errors, status: :unprocessable_entity }
  #      end
  #    end
  #  end
  #
  #  # PUT /articles/1
  #  # PUT /articles/1.json
  #  def update
  #    @article = Article.find(params[:id])
  #
  #    respond_to do |format|
  #      if @article.update_attributes(params[:article])
  #        format.html { redirect_to @article, notice: 'Article was successfully updated.' }
  #        format.json { head :no_content }
  #      else
  #        format.html { render action: "edit" }
  #        format.json { render json: @article.errors, status: :unprocessable_entity }
  #      end
  #    end
  #  end
  #
  #  # DELETE /articles/1
  #  # DELETE /articles/1.json
  #  def destroy
  #    @article = Article.find(params[:id])
  #    @article.destroy
  #
  #    respond_to do |format|
  #      format.html { redirect_to articles_url }
  #      format.json { head :no_content }
  #    end
  #  end
end
